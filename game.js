$('#start').on('click', function () {

  //Получить все игровые кнопки
  var buttons = $('.gameBtn');

  //Сколько чисел в последовательности будет
  var count = $('#level').val();

  //Сколько раз ошиблись
  var errCount = 0;

  //Текущий искомый номер
  var current = 1;

  //Кнопки с номерами
  var gameButtons = [];

  while (gameButtons.length < count) {
    //Берем рандомный индекс от 0 до длины массива buttons(16)
    var index = Math.floor(Math.random() * (buttons.length));
    //Присваиваем кнопку из массива buttons с рандомным индексом
    var button = buttons[index];
    //Проверяем, что этой кнопки еще нет в массиве gameButtons
    if (!gameButtons.includes(button)) {
      //Кладет кнопку, в массив gameButtons
      gameButtons.push(button);
    }
  }

  for (i = 0; i < gameButtons.length; i++) {
    //Сделать кнопки зеленого цвета
    $(gameButtons[i]).addClass('show');
    //Присвоить им текст, равный их порядковому номеру
    $(gameButtons[i]).text(i + 1);
  }

  setTimeout(
    function () {
      //Вернуть кнопки к прежнему виду
      for (i = 0; i < gameButtons.length; i++) {
        $(gameButtons[i]).removeClass('show');
      }

      //Обработчик на клик кнопки
      $('.gameBtn').on('click', function () {
        //this - вернет тот элемент, на который я нажала
        //text() - вернет текст кнопки
        //Сохранить текст кнопки в переменную btnNumber
        var btnNumber = $(this).text();
        //Совпадает ли номер кнопки с искомым номером
        if (btnNumber == current) {
          //значит кликнули правильно
          //this - вернет тот элемент, на который я нажала
          //Тому элементу на который я нажала добавить класс show
          $(this).addClass('show');
          //this - вернет тот элемент, на который я нажала
          $(this).removeClass('error');
          //Увеличиваем текущий номер на единицу, чтобы перейти к следующему
          current++;
        } else {
          //this - вернет тот элемент, на который я нажала
          $(this).addClass('error');
          errCount++;
        }

        if (current - 1 == count) {
          //игра закончилась 
          alert('Поздравляем, вы выиграли!');
          //Перезагрузить страницу
          window.location.reload();
        } else {
          //errCount - количество ошибок
          if (errCount == 3) {
            alert('К сожалению, вы проиграли...');
            //Перезагрузить страницу
            window.location.reload();
          }
        }
      });
    }, count < 7 ? count * 1000 : 7000
  );
});